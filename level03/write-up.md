### Explanations
First of all, we can see that there is a __call to system()__ with the argument "/bin/sh" in the function __decrypt()__.

This call can only be reached if the argument passed to _decrypt()_ is equal to a constant after some calculations. The question is then, how much control do we have over this argument ?
The _decrypt()_ function can only be called from _test()_. Also, the argument passed to _decrypt()_ depends on a __switch statement__.

Before reaching _test()_, the _main()_ function asks us to provide a password, which will be stored as an int thanks to _scanf()_. This int will be __subtracted to the constant 0x1337d00d__, and the result will then be used for the aforementioned switch statement.

In this switch, the only non-default cases are intergers from 1 to 9 or integers from 16 to 21.

We can now try out those cases by providing a password equal to the __decimal equivalent of 0x1337d00d minus the integer we want to pass to _decrypt()___.
_As an example, if we wanted to call decrypt() with the argument '1', we would need to give a password equal to '322424845 - 1'._

By testing all all the possibilities, we see that '322424827' _(322424845 - 18)_ opens a shell.

### Flag
`kgv3tkEb9h2mLkRsPkXRfc2mHbjMxQzvb2FrgKkf`
