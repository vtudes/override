### Explanations
From the first glance, we can see that this level is __vulnerable to format string exploits__.
But we can also take note of the fact that, unlike previous levels, there is __nothing for us to read__ in the stack. In fact, we will have to find a way to __write an arbitrary value at and arbitrary address__.

We will use the __'n' conversion specifier__ of _printf()_ to do so. Here is the description of this specifier, form the man page for _printf()_:
>The number of characters written so far is stored into the integer indicated by the int * (or variant) pointer argument. No argument is converted.

Taking note of that, we can abuse the __padding options__ of _printf()_ to write __as many characters__ as we need at the address used as argument.

For our exploit, we will need two addresses:
	- The address of the exit relocation:	0x080497e0
	- The address of the PAYLOAD variable:	0xffffdee4
_(Here, we are using the address of the exit() relocation because the vulnerable call to printf() is directly followed by a call to exit(). We will replace its address by the address of a shellcode stored in an environment variable.)_

To write the address of PAYLOAD in memory, we would need to ask _printf()_ to write 4294958820 characters... This looks like a lot, maybe we could split that in half.
For that, we will use the __'h' length modifier__ to write a short in stead of an int. This will __split our address in half__, and we'll then just have to write _0xFFFF0000_ and _0xDEE4_ separately at two different addresses, __offset by two bytes__.

Here is how our final payload looks like:
```
$ python -c 'print "\xe2\x97\x04\x08" + "\xe0\x97\x04\x08" + "%57052x" + "%11$hn" + "%8475x" + "%10$hn"' > /tmp/payload
$ cat /tmp/payload - | ./level05
```

### Flag
`h4GtNnaMs2kZFN92ymTr2DcJHAzMfzLW25Ep59mq`
