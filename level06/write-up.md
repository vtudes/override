### Explaining
This is, once more, a program in which you have to log in. This time with a serial number in stead of a password.
We also note that a valid login __must be strictly longer than 5 characters__.

The expected serial number is computed at run time __according to the login__. This means that for any given login, we should be able to retrieve the correct serial number by __setting a breakpoint before the comparison__ between the valid serial number and the one provided by the user.

There is one issue before we can do that though: a call to _ptrace()_ __prevents us from tampering with the execution flow__ with a debugger. To work around that, we have to __change the return value of _ptrace()___ to '0' (easily done by setting eax to '0' after returning from _ptrace()_ in gdb).

Following this workflow, we can now __break the execution right before the comparison__ and get the valid serial for any given login.

Finally, for the login "AAAAAA", we now are now able to give the right serial number, which is '6229082'.

### Flag
`GbcPDRgsFK77LNnnuh7QyFYA2942Gp8yKj9KrWD8`
