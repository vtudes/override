### Explanations
This program functions like a basic interface allowing users to fill or read a table of integers __with a set of commands__.
Here are the available commands:
	- read: this commands asks for an index, and then __displays the content of _table[index]___.
	- store: this commands asks for an index and a value. It then __stores said value in _table[index]_, only if index is not a multiple of 3__.

At the very beginning of the main, all its arguments and environment variables are zero'ed out, so we can't have any shellcode in an environment variable.

We can note that while the table only contains 100 integers, it __does not check if the indices provided by the user are greater than 99__. This means we can read values that are not in the table but __farther up the stack__.
Also, the _read_ command can print the values stored at __negative indices__.

Given the fact that a store command tries to store the number at the given index with a simple `table + (index * 4) = number`, we can basically __ignore the rule about multiples of 3 via an integer overflow__.
Indeed, if you need to write at the index 3, you can simply use the formula `(2pow(32) + (4n))/4` to get __another index for the same location__.

If we set a breakpoint at the call to _store_number()_, we see on top of stack the __address of the table itself__. It is then possible to __dereference it__ to get the address of the first integer of that table.
If we subtract those addresses we get the following: `0xffffd510 - 0xffffd534 = -(0x24)`. This value is the difference __in bytes__ between the two but this is an integer table... Meaning that we should divide that value by four, and get -9. Therefore, by __reading the content of table[-9]__, we can at any point get the address of the table itself.

If we __store our shellcode in the first indices of the table__, and manage to jump onto the address at table[-9], we should get a shell!

In gdb, we will then use `info frame` (after hitting the breakpoint we set previously). In the output of this command, we can then __retrieve the address of the saved eip__.
With the same operation as precedently, we can do `(0xffffd6fc - 0xffffd534) / 4` and convert the result in decimal to get the index of the saved eip: 114.

We now have control over the execution flow!

Let's put all that together. Using the commands at your disposal, you should fill the table with the following values:
| Index									| Value								|
|---------------------------------------|-----------------------------------|
| 1073741824 _(overflowing into 0)_		| 191547697							|
| 1										| 1750243672						|
| 2										| 1752379183						|
| 1073741827 _(overflowing into 3)_		| 1768042344						|
| 4										| 3454241134						|
| 5										| 128								|
| ...									| ...								|
| 1073741938 _(overflowing into 114)_	| _address found at the index -9_	|


Once everything is set, just quit and get a fresh shell as level08.

### Flag
`7WJ6jFBzrcjEYXudxnM3kdW7n3qyxR6tk2xGrkSC`
