### Explanations
When disassembling the binary, we can see that it only contains a `scanf()`, and a comparison.

This scanf gets user input in the form of an int. The program then simply __compares said int to 0x149__. If this comparison is true, "/bin/sh" is then called. _(As all binaries on this box, "./level00" has the SUID bit set. Meaning that we get a shell as the user "level01")_

0x149 is equivalent to 5276 in decimal. Therefore we should be able to get a shell with the following commands:

```
$ ./level00
Password: 5276
```

A shell opens, we just have to retrieve the flag: 
```
$ cd ../level01
$ cat .pass
```

### Flag
`uSq2ehEGT6c9S24zbshexZQBXUGrncxn5sD5QfGL`
