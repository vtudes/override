### Explanations
This is not an exploit for a know vulnerability, we are here just taking advantage of a programming mistake.

The executable at hand is a file backup utility. In order to create a backup of a file, it does the following:
	- Opens the file ./backups/.log _(to log eventual error messages inside it.)_
	- Opens the file given as an argument _(as the user "level09" due to the suid)_.
	- Reads the contents of the second file.
	- Tries to create a copy of this file _(same name and contents)_ inside the backups folder.
	- Grants read permissions for this file to the "users" group _(in which all users are present)_.

Given the fact that we have full access to anything inside the "/tmp" folder, we can simply do the following to get the flag for the next level:
```
$ cd /tmp
$ mkdir -p backups/home/users/level09
$ /home/users/level08/level08 /home/users/level09/.pass
$ cat backups/home/users/level09/.pass
```

And we get the flag.

### Flag
`fjAwpJNs2vvkFLRebEvAQ2hFZ4uQBWfHRsP62d8S`
