### Explanations
Once again, we are asked for a username/password combination. But this time, our password directly gets compared to the flag. To do so, __the program keeps a copy of the flag in memory__ which might be to our advantage.

In case of an invalid password, there is a __bad use of printf__, which is directly using the username buffer as it's only argument. Making it vulnerable to a __format string exploit__.
Indeed, by dumping what's on the stack, we may be able to print out the string that contains the expected password _(previously collected in level03's ".pass" file)_.

In order to dump the stack, please find the python script in the resources folder of this level.

After analysing the output of our script, we can now see that the __lines 22 to 26 contain contiguous blocks of hex characters__. This might very well be our password.
We now want to translate those in ASCII, and put them in the right endianness.
To do so, we can do the following in a python interpreter:
```
>>> "756e505234376848".decode("hex")[::-1]
'Hh74RPnu'
>>> "45414a3561733951".decode("hex")[::-1]
'Q9sa5JAE'
>>> "377a7143574e6758".decode("hex")[::-1]
'XgNWCqz7'
>>> "354a35686e475873".decode("hex")[::-1]
'sXGnh5J5'
>>> "48336750664b394d".decode("hex")[::-1]
'M9KfPg3H'
```

We just have to create a single string using all of those, recreating the initial flag.

### Flag
`Hh74RPnuQ9sa5JAEXgNWCqz7sXGnh5J5M9KfPg3H`
